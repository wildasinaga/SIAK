-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 10 Apr 2019 pada 04.26
-- Versi Server: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `siakbaru`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `bentuk_pelanggaran`
--

CREATE TABLE `bentuk_pelanggaran` (
  `id_bentuk_pelanggaran` int(11) NOT NULL,
  `nama_bentuk_pelanggaran` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `bentuk_pelanggaran`
--

INSERT INTO `bentuk_pelanggaran` (`id_bentuk_pelanggaran`, `nama_bentuk_pelanggaran`) VALUES
(1, 'Asrama'),
(2, 'Kampus'),
(3, 'Kantin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `bid_akademik`
--

CREATE TABLE `bid_akademik` (
  `id_akademik` int(11) NOT NULL,
  `email` varchar(25) NOT NULL,
  `nama` varchar(25) NOT NULL,
  `nik` varchar(15) NOT NULL,
  `id_user` int(3) NOT NULL,
  `status` varchar(255) DEFAULT 'Bidang Akademik'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `bid_akademik`
--

INSERT INTO `bid_akademik` (`id_akademik`, `email`, `nama`, `nik`, `id_user`, `status`) VALUES
(2, 'melani@gmai.com', 'Melani Tambun', '54321', 4, 'Bidang Akademik'),
(3, 'vero@gmail.com', 'Vero Sinaga', 'wq1231', 12, 'Bidang Akademik');

-- --------------------------------------------------------

--
-- Struktur dari tabel `bid_keasramaan`
--

CREATE TABLE `bid_keasramaan` (
  `id_keasramaan` int(11) NOT NULL,
  `nik` varchar(15) NOT NULL,
  `email` varchar(25) NOT NULL,
  `nama` varchar(25) NOT NULL,
  `id_user` int(3) NOT NULL,
  `status` varchar(255) DEFAULT 'Bidang Keasramaan'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `bid_keasramaan`
--

INSERT INTO `bid_keasramaan` (`id_keasramaan`, `nik`, `email`, `nama`, `id_user`, `status`) VALUES
(2, '12345', 'naomi@gmail.com', 'Naomi Nainggolan', 3, 'Bidang Keasramaan'),
(3, '123234', 'safiah@gmail.com', 'Safiah Sitorus', 13, 'Bidang Keasramaan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `buku_pedoman`
--

CREATE TABLE `buku_pedoman` (
  `id_buku` int(8) NOT NULL,
  `tanggal_update` varchar(200) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `file` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `buku_pedoman`
--

INSERT INTO `buku_pedoman` (`id_buku`, `tanggal_update`, `judul`, `file`) VALUES
(2, '0000-00-00', 'test', 'uploads/bupati.png'),
(3, '0000-00-00', 'Upload Buku Pedoman', 'uploads/12S15050_W05S03_SD_Wilda Sinaga.pdf'),
(4, '06/06/2018', 'Upload Buku Pedoman lagi', 'uploads/bupati.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `daftar_pelanggaran`
--

CREATE TABLE `daftar_pelanggaran` (
  `id_pelanggaran` int(11) NOT NULL,
  `jenis_pelanggaran` int(11) NOT NULL,
  `bentuk_pelanggaran` int(11) NOT NULL,
  `nama_pelanggaran` text NOT NULL,
  `poin_pelanggaran` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `daftar_pelanggaran`
--

INSERT INTO `daftar_pelanggaran` (`id_pelanggaran`, `jenis_pelanggaran`, `bentuk_pelanggaran`, `nama_pelanggaran`, `poin_pelanggaran`) VALUES
(1, 1, 1, 'Menyembunyikan Hanger', 5),
(2, 1, 1, 'Terlambat Kurve', 5),
(3, 1, 1, 'Membawa sepatu ke kamar', 5),
(4, 3, 1, 'Membakar asrama', 100);

-- --------------------------------------------------------

--
-- Struktur dari tabel `dosen`
--

CREATE TABLE `dosen` (
  `id_dosen` int(11) NOT NULL,
  `nik` varchar(15) NOT NULL,
  `nama` varchar(25) NOT NULL,
  `id_user` int(3) NOT NULL,
  `status` varchar(255) DEFAULT 'Dosen'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `dosen`
--

INSERT INTO `dosen` (`id_dosen`, `nik`, `nama`, `id_user`, `status`) VALUES
(2, '21435', 'sarah tambunan', 8, 'Dosen'),
(3, '12342', 'Donna Ria', 14, 'Dosen');

-- --------------------------------------------------------

--
-- Struktur dari tabel `izin_mhs`
--

CREATE TABLE `izin_mhs` (
  `surat_cuti_id` int(11) NOT NULL,
  `id_keasramaan` int(11) NOT NULL,
  `nim` varchar(255) NOT NULL,
  `nama_mhs` varchar(255) DEFAULT NULL,
  `kelas` varchar(255) DEFAULT NULL,
  `id_kategori` int(11) NOT NULL,
  `tanggal_berangkat` varchar(10) NOT NULL,
  `tanggal_kembali` varchar(10) NOT NULL,
  `alasan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `izin_mhs`
--

INSERT INTO `izin_mhs` (`surat_cuti_id`, `id_keasramaan`, `nim`, `nama_mhs`, `kelas`, `id_kategori`, `tanggal_berangkat`, `tanggal_kembali`, `alasan`) VALUES
(1, 2, '12S15046', 'WIlda', '13SI2', 1, '05/30/2018', '05/22/2018', 'Sakit Deman dan Batuk'),
(2, 2, '12S15046', 'novi gultom', '13SI2', 1, '05/26/2018', '05/27/2018', 'Opname'),
(3, 2, '12S15046', 'novi gultom', '13SI2', 2, '05/30/2018', '05/31/2018', 'Mengurus Paspor'),
(4, 3, '12S15029', 'Christine Sibuea', '13SI1', 1, '06/05/2018', '06/05/2018', 'Sakit kepala sebelah');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis_pelanggaran`
--

CREATE TABLE `jenis_pelanggaran` (
  `id_jenis_pelanggaran` int(11) NOT NULL,
  `nama_jenis_pelanggaran` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jenis_pelanggaran`
--

INSERT INTO `jenis_pelanggaran` (`id_jenis_pelanggaran`, `nama_jenis_pelanggaran`) VALUES
(1, 'Pelanggaran Ringan'),
(2, 'Pelanggaran Sedang'),
(3, 'Pelanggaran Berat');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori_izin`
--

CREATE TABLE `kategori_izin` (
  `id_kategori` int(11) NOT NULL,
  `deskripsi` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kategori_izin`
--

INSERT INTO `kategori_izin` (`id_kategori`, `deskripsi`) VALUES
(1, 'sakit'),
(2, 'izin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `log_activity`
--

CREATE TABLE `log_activity` (
  `id_log` int(8) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `nim` varchar(255) DEFAULT NULL,
  `tanggal` varchar(255) NOT NULL,
  `deskripsi` longtext NOT NULL,
  `file_log` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `log_activity`
--

INSERT INTO `log_activity` (`id_log`, `id_user`, `nim`, `tanggal`, `deskripsi`, `file_log`) VALUES
(1, 6, '12S15046', '03/13/2019', 'dfgfg', 'uploads/logactivity/siakbaru.sql');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `nim` varchar(255) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_keasramaan` int(11) DEFAULT NULL,
  `nama_mhs` varchar(255) NOT NULL,
  `kelas` varchar(255) NOT NULL,
  `id_dosen_wali` int(11) NOT NULL,
  `angkatan` int(11) NOT NULL,
  `foto` blob NOT NULL,
  `poin_pelanggaran` int(11) DEFAULT NULL,
  `status` varchar(255) DEFAULT 'Mahasiswa'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mahasiswa`
--

INSERT INTO `mahasiswa` (`nim`, `id_user`, `id_keasramaan`, `nama_mhs`, `kelas`, `id_dosen_wali`, `angkatan`, `foto`, `poin_pelanggaran`, `status`) VALUES
('12S15029', 15, 2, 'Christine Sibuea', '13SI1', 2, 2015, '', 80, 'Mahasiswa'),
('12S15046', 6, 2, 'novi gultom', '13SI2', 2, 2015, '', 75, 'Mahasiswa');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pelanggaran_mahasiswa`
--

CREATE TABLE `pelanggaran_mahasiswa` (
  `id_pelanggaran` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nim` varchar(255) NOT NULL,
  `id_daftarpelanggaran` int(11) NOT NULL,
  `id_dosen` int(11) NOT NULL,
  `kelas` varchar(255) NOT NULL,
  `nama_mahasiswa` varchar(255) NOT NULL,
  `deskripsi` text NOT NULL,
  `id_jenis_pelanggaran` int(11) NOT NULL,
  `status` varchar(16) DEFAULT NULL,
  `tanggal_pelanggaran` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pelanggaran_mahasiswa`
--

INSERT INTO `pelanggaran_mahasiswa` (`id_pelanggaran`, `id_user`, `nim`, `id_daftarpelanggaran`, `id_dosen`, `kelas`, `nama_mahasiswa`, `deskripsi`, `id_jenis_pelanggaran`, `status`, `tanggal_pelanggaran`) VALUES
(1, 6, '12S15046', 1, 2, '', '', 'sdf', 1, NULL, ''),
(2, 3, '12S15046', 1, 2, 'SI', 'Novi', 'Test', 1, 'Approve', ''),
(3, 3, '12S15046', 1, 2, 'SI', 'Novi', 'Tes', 1, 'Approve', ''),
(4, 3, '12S15046', 1, 2, 'Si', 'Novi', 'Tes', 1, 'Approve', ''),
(5, 3, '12S15046', 1, 2, 'S', 'Novi', 'Tes', 1, 'Approve', ''),
(6, 3, '12S15046', 1, 2, 'SI', 'Novi', 'Test', 1, 'Approve', ''),
(7, 3, '12S15046', 1, 2, 'SI', 'Novi', 'Tes', 1, 'Approve', ''),
(8, 3, '12S15029', 1, 2, '13SI1', 'Christine Sibuea', 'Menyembunyikan hanger sebanyak 5 buah', 1, 'Approve', ''),
(9, 3, '12S15029', 1, 2, '13SI1', 'Christine Sibuea', 'Sebanyak 100 buah', 1, 'Approve', '05/30/2018'),
(10, 3, '12S15029', 2, 2, '13SI1', 'Christine Sibuea', 'Telat Kurve 2 jam', 1, 'Approve', '05/30/2018'),
(11, 3, '12S15046', 1, 2, '13SI2', 'novi gultom', 'menyembunyikan hanger sebanyak 5 buah di kamar', 1, 'Approve', '06/22/2018'),
(12, 3, '12S15029', 3, 2, '13SI1', 'Christine Sibuea', 'Tes', 1, 'Approve', '03/16/2019'),
(13, 3, '12S15046', 3, 2, '13SI2', 'novi gultom', 'asd', 1, 'Approve', '03/12/2019'),
(14, 3, '12S15046', 1, 2, '13SI2', 'novi gultom', 'Berat', 1, 'Approve', '03/07/2019'),
(15, 3, '12S15046', 4, 2, '13SI2', 'novi gultom', 'berat ces', 3, 'Not Approve', '03/08/2019');

-- --------------------------------------------------------

--
-- Struktur dari tabel `poin_tambahan`
--

CREATE TABLE `poin_tambahan` (
  `id_poin_tambahan` int(11) NOT NULL,
  `nim` varchar(255) NOT NULL,
  `nama_mhs` varchar(255) NOT NULL,
  `kelas` varchar(255) NOT NULL,
  `alasan` varchar(500) NOT NULL,
  `poin_tambahan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `security`
--

CREATE TABLE `security` (
  `id_security` int(11) NOT NULL,
  `nik` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `id_user` int(11) NOT NULL,
  `status` varchar(255) DEFAULT 'Security'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `security`
--

INSERT INTO `security` (`id_security`, `nik`, `nama`, `id_user`, `status`) VALUES
(2, '98765', 'fanta sibarani', 10, 'Security');

-- --------------------------------------------------------

--
-- Struktur dari tabel `siak_user`
--

CREATE TABLE `siak_user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `auth_key` varchar(255) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `password_user_token` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `status` smallint(6) DEFAULT '10',
  `created_at` int(11) DEFAULT NULL,
  `update_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `siak_user`
--

INSERT INTO `siak_user` (`id_user`, `username`, `auth_key`, `password_hash`, `password_user_token`, `email`, `status`, `created_at`, `update_at`) VALUES
(1, 'Putri M', 'vBVSIVKQyKGRymUULRR3MxP2HNn-K82M', '$2y$13$dTCWkgdkjODfcJU4NQX7Q.ySmf7tTIef0V28duNzUs4XkaR/htumG', NULL, 'putrimanurung@gmail.com', 10, NULL, NULL),
(3, 'Naomi Nainggolan', '2rL1vOvCsP9UMU2fz9JwAfy34MJGM7z4', '$2y$13$PfNV3tedIgklDhyzmJ1udebtztQsPdq93E3LDMM0mRj1nwVU/X3vO', NULL, 'nainggolannaomi@gmail.com', 10, NULL, NULL),
(4, 'Melani Tambun', '7jdBPooDYKAJNAVKvHPztynaIP56uKrC', '$2y$13$Cm7S1ipS1hhv4BXdF225l.dlKKAYdOtmwd.Oya/Yjk2jN1.9NhUAq', NULL, 'tambunmelani@gmail.com', 10, NULL, NULL),
(6, 'novi gultom', '54pm6q0tTitzqHDZHpSJ0O_-Yi9Dyr4g', '$2y$13$4ribC.UBWNmulzYD7B00MO4La1gDLkK2shhYd7D4Ot88x6DCjzDQS', NULL, 'novigultom@gmail.com', 10, NULL, NULL),
(8, 'sarah tambunan', 'SCSVO_hcat7eXJIKn9B2nH4IGzmHzIab', '$2y$13$sYHl2TjSFmB38B1TjEzB0.gmCmzKzRVhe5Y7v498YQxYyu6Sfixzq', NULL, 'sarahtambunan@gmail.com', 10, NULL, NULL),
(9, 'wilda sinaga', 'zWyNTczz1f01YMd9PT6CtwMThdq3kXPN', '$2y$13$gT1ndca3VWKZMGjhGDSViOUBJwrkTHxZdevSfQJyWIKjGOzSZdys6', NULL, 'wildasinaga@gmail.com', 10, NULL, NULL),
(10, 'fanta sibarani', 'HM5tUsZm8cSi53fhu1RcdazvWw5byOuY', '$2y$13$Kz9ezVEQXrMS8mBWOm43ueFQRdkFPCV898DWt1ugaRSrt5as22r1C', NULL, 'fantasibarani@gmail.com', 10, NULL, NULL),
(11, 'aldoni hutapea', 'dct2x-G3IFg16AOCeBtbcCiTtuq6lM7F', '$2y$13$ZhKjYVZwWGBH.4ATyXzTtefbKdbKY1RaL8cfr1i.GyBR0yb7XMV0q', NULL, 'aldonihutapea@gmail.com', 10, NULL, NULL),
(12, 'veronika sinaga', 'y3AOOXmZsR1IbNpE1CKLVr-WPLmp5p0i', '$2y$13$Pf95gXSda0m6nnbjPEJX/.CjFSPM15CBPQlc2ji7xbbAxIEbysDQq', NULL, 'veronikasinaga@gmail.com', 10, NULL, NULL),
(13, 'safiah sitorus', 'YcAi4qiy1hbYMhQFozSGck0XnUjR6Owd', '$2y$13$hmQmglQDbkxAYdnLfKg6/eQ5MbRsMrXkIXCrxipiTzwz6oFCeSfRO', NULL, 'safiahsitorus@gmail.com', 10, NULL, NULL),
(14, 'donna ria', 'GwNLJyWw8DG6jkXM_dtZIlDOho56jYF-', '$2y$13$3InAJ40hv9rwOp9rJPVwD.7nyjn3ov2TBk19qkSllzVOPPjDPTyFC', NULL, 'donnaria@gmail.com', 10, NULL, NULL),
(15, 'christine sibuea', 'Gxh_kT2Fv19YypMpm8p1fJuv-i9cM8Ql', '$2y$13$y/cVm3S6nYCgmEPMXAUNuOi8WonYCBhA3BIDPgLm/0BPdtTXvepdm', NULL, 'christinesibuea@gmail.com', 10, NULL, NULL),
(16, 'jelita siahaan', 'VB-dvNS6MbVTBezyw3PLdE4xjdjXox_r', '$2y$13$McJxVRm6BtUcSHOYYYcPPO3R1sfRcMbaIcAPegxbThwcrXfxkCwQK', NULL, 'jelitasiahaan@gmail.com', 10, NULL, NULL),
(17, 'wilda', 'T-ClshyLUk03ctgeE4uJxZtPj-BAt3pR', '$2y$13$tkLtbsnOGtVVm3efyJSklOFOq3YHx6PAtx7E/QyOJOeV2ntIGZY12', NULL, 'wildasinaga7@gmail.com', 10, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
(1, 'putrimanurung', 'a5o-jC6MXel5MXDiD0-xEpDvQDbaMIcn', '$2y$13$bIDtOpY447l25XDvolXVP.QEBeEaMJ75NNp0Y/RH9w0LHDbFmPyNC', NULL, 'putrimanurung60@gmail.com', 10, 1524040255, 1524040255),
(2, 'putrim', 'gKrDwxX0PQDobec9W2xfEnOptB3JZmHU', '$2y$13$D4KWnDPgd.6F9IDrs7TGGeWdo/5mbp.KuTk9kJcnrVZNVquYDkuw6', NULL, 'putrimanurungy60@gmail.com', 10, 1524633853, 1524633853),
(3, 'putri', 'k4meffQvinASFooV4j7aPPlUX2_BGQUb', '$2y$13$oWCy6SHYJMMoZtFdAfzuDOgi.fYCjwSyhMKqcDIeiMPa7NaWk1L/C', NULL, 'putrimanurun60@gmail.com', 10, 1525155366, 1525155366),
(4, 'admin123', 'V6ChizxnYL4ATs0dNZpaEGRTWATI74fV', '$2y$13$GLSYZ98L9xaNjmbVoSeRqO6L0jYP5.nOfbmM3P0JwSmvkxfrn6TFC', NULL, 'admin@mail.com', 10, 1525338482, 1525338482),
(5, 'admin1234', 'R0nzGzZ9rfFyKWI0l0sW2-tpVMtfG6N5', '$2y$13$yUnoIX9S6AGa19fAjW/3oeSLvZSPH1quuKvviy8u6Srpz5FPmclkG', NULL, 'admin123@gmail.com', 10, 1525830428, 1525830428);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bentuk_pelanggaran`
--
ALTER TABLE `bentuk_pelanggaran`
  ADD PRIMARY KEY (`id_bentuk_pelanggaran`);

--
-- Indexes for table `bid_akademik`
--
ALTER TABLE `bid_akademik`
  ADD PRIMARY KEY (`id_akademik`),
  ADD KEY `cnst_akademik_user` (`id_user`);

--
-- Indexes for table `bid_keasramaan`
--
ALTER TABLE `bid_keasramaan`
  ADD PRIMARY KEY (`id_keasramaan`),
  ADD KEY `const_asrama_user` (`id_user`);

--
-- Indexes for table `buku_pedoman`
--
ALTER TABLE `buku_pedoman`
  ADD PRIMARY KEY (`id_buku`);

--
-- Indexes for table `daftar_pelanggaran`
--
ALTER TABLE `daftar_pelanggaran`
  ADD PRIMARY KEY (`id_pelanggaran`),
  ADD KEY `jenis_pelanggaran` (`jenis_pelanggaran`),
  ADD KEY `bentuk_pelanggaran` (`bentuk_pelanggaran`);

--
-- Indexes for table `dosen`
--
ALTER TABLE `dosen`
  ADD PRIMARY KEY (`id_dosen`),
  ADD KEY `Const_user_dosen` (`id_user`);

--
-- Indexes for table `izin_mhs`
--
ALTER TABLE `izin_mhs`
  ADD PRIMARY KEY (`surat_cuti_id`),
  ADD KEY `izin_mhs_ibfk_1` (`id_keasramaan`),
  ADD KEY `izin_mhs_ibfk_2` (`id_kategori`),
  ADD KEY `nim` (`nim`);

--
-- Indexes for table `jenis_pelanggaran`
--
ALTER TABLE `jenis_pelanggaran`
  ADD PRIMARY KEY (`id_jenis_pelanggaran`);

--
-- Indexes for table `kategori_izin`
--
ALTER TABLE `kategori_izin`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `log_activity`
--
ALTER TABLE `log_activity`
  ADD PRIMARY KEY (`id_log`);

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`nim`),
  ADD KEY `Const_user_mahasiswa` (`id_user`),
  ADD KEY `Const_mahasiswa_keasramaan` (`id_keasramaan`),
  ADD KEY `Const_dosen_mahasiswa` (`id_dosen_wali`);

--
-- Indexes for table `pelanggaran_mahasiswa`
--
ALTER TABLE `pelanggaran_mahasiswa`
  ADD PRIMARY KEY (`id_pelanggaran`),
  ADD KEY `costrain1` (`id_user`),
  ADD KEY `constrain2` (`nim`),
  ADD KEY `constrain3` (`id_daftarpelanggaran`),
  ADD KEY `constrain4` (`id_dosen`),
  ADD KEY `constrain5` (`id_jenis_pelanggaran`);

--
-- Indexes for table `poin_tambahan`
--
ALTER TABLE `poin_tambahan`
  ADD PRIMARY KEY (`id_poin_tambahan`),
  ADD KEY `nim` (`nim`);

--
-- Indexes for table `security`
--
ALTER TABLE `security`
  ADD PRIMARY KEY (`id_security`),
  ADD KEY `Const_user_security` (`id_user`);

--
-- Indexes for table `siak_user`
--
ALTER TABLE `siak_user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bentuk_pelanggaran`
--
ALTER TABLE `bentuk_pelanggaran`
  MODIFY `id_bentuk_pelanggaran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `bid_akademik`
--
ALTER TABLE `bid_akademik`
  MODIFY `id_akademik` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `bid_keasramaan`
--
ALTER TABLE `bid_keasramaan`
  MODIFY `id_keasramaan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `buku_pedoman`
--
ALTER TABLE `buku_pedoman`
  MODIFY `id_buku` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `daftar_pelanggaran`
--
ALTER TABLE `daftar_pelanggaran`
  MODIFY `id_pelanggaran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `dosen`
--
ALTER TABLE `dosen`
  MODIFY `id_dosen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `izin_mhs`
--
ALTER TABLE `izin_mhs`
  MODIFY `surat_cuti_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `jenis_pelanggaran`
--
ALTER TABLE `jenis_pelanggaran`
  MODIFY `id_jenis_pelanggaran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `log_activity`
--
ALTER TABLE `log_activity`
  MODIFY `id_log` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `pelanggaran_mahasiswa`
--
ALTER TABLE `pelanggaran_mahasiswa`
  MODIFY `id_pelanggaran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `poin_tambahan`
--
ALTER TABLE `poin_tambahan`
  MODIFY `id_poin_tambahan` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `security`
--
ALTER TABLE `security`
  MODIFY `id_security` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `siak_user`
--
ALTER TABLE `siak_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `bid_akademik`
--
ALTER TABLE `bid_akademik`
  ADD CONSTRAINT `cnst_akademik_user` FOREIGN KEY (`id_user`) REFERENCES `siak_user` (`id_user`);

--
-- Ketidakleluasaan untuk tabel `bid_keasramaan`
--
ALTER TABLE `bid_keasramaan`
  ADD CONSTRAINT `const_asrama_user` FOREIGN KEY (`id_user`) REFERENCES `siak_user` (`id_user`);

--
-- Ketidakleluasaan untuk tabel `daftar_pelanggaran`
--
ALTER TABLE `daftar_pelanggaran`
  ADD CONSTRAINT `daftar_pelanggaran_ibfk_1` FOREIGN KEY (`jenis_pelanggaran`) REFERENCES `jenis_pelanggaran` (`id_jenis_pelanggaran`),
  ADD CONSTRAINT `daftar_pelanggaran_ibfk_2` FOREIGN KEY (`bentuk_pelanggaran`) REFERENCES `bentuk_pelanggaran` (`id_bentuk_pelanggaran`);

--
-- Ketidakleluasaan untuk tabel `dosen`
--
ALTER TABLE `dosen`
  ADD CONSTRAINT `Const_user_dosen` FOREIGN KEY (`id_user`) REFERENCES `siak_user` (`id_user`);

--
-- Ketidakleluasaan untuk tabel `izin_mhs`
--
ALTER TABLE `izin_mhs`
  ADD CONSTRAINT `izin_mhs_ibfk_1` FOREIGN KEY (`id_keasramaan`) REFERENCES `bid_keasramaan` (`id_keasramaan`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `izin_mhs_ibfk_2` FOREIGN KEY (`id_kategori`) REFERENCES `kategori_izin` (`id_kategori`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `izin_mhs_ibfk_3` FOREIGN KEY (`nim`) REFERENCES `mahasiswa` (`nim`);

--
-- Ketidakleluasaan untuk tabel `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD CONSTRAINT `Const_dosen_mahasiswa` FOREIGN KEY (`id_dosen_wali`) REFERENCES `dosen` (`id_dosen`),
  ADD CONSTRAINT `Const_mahasiswa_keasramaan` FOREIGN KEY (`id_keasramaan`) REFERENCES `bid_keasramaan` (`id_keasramaan`),
  ADD CONSTRAINT `Const_user_mahasiswa` FOREIGN KEY (`id_user`) REFERENCES `siak_user` (`id_user`);

--
-- Ketidakleluasaan untuk tabel `pelanggaran_mahasiswa`
--
ALTER TABLE `pelanggaran_mahasiswa`
  ADD CONSTRAINT `constrain2` FOREIGN KEY (`nim`) REFERENCES `mahasiswa` (`nim`),
  ADD CONSTRAINT `constrain3` FOREIGN KEY (`id_daftarpelanggaran`) REFERENCES `daftar_pelanggaran` (`id_pelanggaran`),
  ADD CONSTRAINT `constrain4` FOREIGN KEY (`id_dosen`) REFERENCES `dosen` (`id_dosen`),
  ADD CONSTRAINT `constrain5` FOREIGN KEY (`id_jenis_pelanggaran`) REFERENCES `jenis_pelanggaran` (`id_jenis_pelanggaran`),
  ADD CONSTRAINT `costrain1` FOREIGN KEY (`id_user`) REFERENCES `siak_user` (`id_user`);

--
-- Ketidakleluasaan untuk tabel `poin_tambahan`
--
ALTER TABLE `poin_tambahan`
  ADD CONSTRAINT `poin_tambahan_ibfk_1` FOREIGN KEY (`nim`) REFERENCES `mahasiswa` (`nim`);

--
-- Ketidakleluasaan untuk tabel `security`
--
ALTER TABLE `security`
  ADD CONSTRAINT `Const_user_security` FOREIGN KEY (`id_user`) REFERENCES `siak_user` (`id_user`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
