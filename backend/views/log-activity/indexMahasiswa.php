<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\LogActivitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Log Activity';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-activity-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Upload Log Activity', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_log',
            'id_user',
            'nim',
            'tanggal',
            'deskripsi:ntext',
             [
            'attribute' => 'file_log',
            'format' => 'raw',
            'value' => function($model){
                return Html::a('Download File', ['download', 'id' => $model->id_log], ['class' => '']);
            }
            ],
            //'file_log',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
