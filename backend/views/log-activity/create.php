<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\LogActivity */

$this->title = 'Upload Log Activity';
$this->params['breadcrumbs'][] = ['label' => 'Log Activity', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-activity-create">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
