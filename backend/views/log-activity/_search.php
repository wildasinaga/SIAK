<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\LogActivitySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="log-activity-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_log') ?>

    <?= $form->field($model, 'id_user') ?>

    <?= $form->field($model, 'nim') ?>

    <?= $form->field($model, 'tanggal') ?>

    <?= $form->field($model, 'deskripsi') ?>

    <?php // echo $form->field($model, 'file_log') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
