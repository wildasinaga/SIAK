<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\LogActivity */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="log-activity-form">

    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>

    

    <?= $form->field($model, 'tanggal')->widget(
        DatePicker::className([
        'model' => $model,
        'attribute' => 'tanggal',
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'options' => ['placeholder' => 'Tanggal Upload ...'],
        'pluginOptions'=> [
            'autoclose'=> true,
            'format'=> 'yyyy-mm-dd'
            ]
        ])

    ) ?>

    <?= $form->field($model, 'deskripsi')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'file_upload')->fileInput() ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
