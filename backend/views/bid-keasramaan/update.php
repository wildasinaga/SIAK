<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\BidKeasramaan */

$this->title = 'Update Bid Keasramaan: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Bid Keasramaans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_keasramaan, 'url' => ['view', 'id' => $model->id_keasramaan]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bid-keasramaan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
