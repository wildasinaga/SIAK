<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\BidKeasramaan */

$this->title = $model->nama;

$this->params['breadcrumbs'][] = ['label' => 'Bid Keasramaans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bid-keasramaan-view">


    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_keasramaan], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_keasramaan], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id_keasramaan',
            'nik',
            'email:email',
            'nama',
            //'id_user',
            'status',
        ],
    ]) ?>

</div>
