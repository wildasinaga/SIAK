<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\BidKeasramaan */

$this->title = 'Create Bid Keasramaan';
$this->params['breadcrumbs'][] = ['label' => 'Bid Keasramaans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bid-keasramaan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
