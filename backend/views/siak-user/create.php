<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\SiakUser */

$this->title = 'Create Siak User';
$this->params['breadcrumbs'][] = ['label' => 'Siak Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="siak-user-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
