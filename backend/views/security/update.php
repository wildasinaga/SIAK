<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Security */

$this->title = 'Update Security: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Securities', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_security, 'url' => ['view', 'id' => $model->id_security]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="security-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
