<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\PoinTambahan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="poin-tambahan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nim')->textInput(['maxlength' => true, 'id'=>'nimmahasiswa']) ?>

    <?= $form->field($model, 'nama_mhs')->textInput(['maxlength' => true, 'readOnly'=>'readOnly']) ?>

    <?= $form->field($model, 'kelas')->textInput(['maxlength' => true, 'readOnly'=>'readOnly']) ?>

    <?= $form->field($model, 'alasan')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'poin_tambahan')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$script = <<< JS
//here you right all your javacript stuff
$('#nimmahasiswa').change(function(){
    var nimmahasiswa = $(this).val();
    
    $.get('index.php?r=poin-tambahan/get-nama-mhs-kelas',{ nimmahasiswa : nimmahasiswa}, function(data){
        var data = $.parseJSON(data);
        ;
        $('#pointambahan-nama_mhs').attr('value', data.nama_mhs);
        $('#pointambahan-kelas').attr('value', data.kelas);
    });
});


JS;
$this->registerJs($script);

?>

