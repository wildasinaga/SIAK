<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\PoinTambahan */

$this->title = 'Tambah Poin Mahasiswa';
$this->params['breadcrumbs'][] = ['label' => 'Poin Tambahans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="poin-tambahan-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
