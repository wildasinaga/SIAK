<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\PoinTambahanSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="poin-tambahan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_poin_tambahan') ?>

    <?= $form->field($model, 'nim') ?>

    <?= $form->field($model, 'nama_mhs') ?>

    <?= $form->field($model, 'kelas') ?>

    <?= $form->field($model, 'alasan') ?>

    <?php // echo $form->field($model, 'poin_tambahan') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
