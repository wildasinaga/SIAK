<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PoinTambahanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Poin Tambahan';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="poin-tambahan-index">

    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Tambah Poin Mahasiswa', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_poin_tambahan',
            'nim',
            'nama_mhs',
            'kelas',
            'alasan',
            //'poin_tambahan',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
