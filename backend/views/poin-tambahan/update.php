<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\PoinTambahan */

$this->title = 'Update Poin Tambahan: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Poin Tambahans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_poin_tambahan, 'url' => ['view', 'id' => $model->id_poin_tambahan]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="poin-tambahan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
