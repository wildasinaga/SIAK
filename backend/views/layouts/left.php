<?php 
use backend\models\Mahasiswa;
use backend\models\Dosen;
use backend\models\Security;
use backend\models\BidAkademik;
use backend\models\BidKeasramaan;
?>
<aside class="main-sidebar">

    <section class="sidebar">
    <?php 
        $user = Yii::$app->user->id;
        $mahasiswa = Mahasiswa::find()->where(['id_user' => $user])->one();
        $dosen = Dosen::find()->where(['id_user' => $user])->one();
        $security = Security::find()->where(['id_user' => $user])->one();
        $keasramaan = BidKeasramaan::find()->where(['id_user' => $user])->one();
        $akademik = BidAkademik::find()->where(['id_user' => $user])->one();
    ?>
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>
                    <?php echo $mahasiswa['nama_mhs'] ?>
                    <?php echo $dosen['nama'] ?>
                    <?php echo $security['nama'] ?>
                    <?php echo $akademik['nama'] ?>
                    <?php echo $keasramaan['nama'] ?>
                </p>

                <a href="#"><i cl

                    ass="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                
              
            </div>
        </form>
        <!-- /.search form -->
        <?php if($mahasiswa['id_user'] == $user) { ?>
        <!-- Daftar Menu Untuk Login sebagai Mahasiswa -->
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    // ['label' => 'Menu Yii2', 'options' => ['class' => 'header']],
                    ['label' => 'Daftar Pelanggaran', 'icon' => 'list', 'url' => ['/daftar-pelanggaran/mahasiswa']],
                    ['label' => 'Pelanggaran Mahasiswa', 'icon' => 'dashboard', 'url' => ['/pelanggaran-mahasiswa/mahasiswa']],
                    //['label' => 'Izin Mahasiswa', 'icon' => ' fa-envelope', 'url' => ['/izin-mhs/index']],
                    ['label' => 'Log Activity', 'icon' => ' fa-users', 'url' => ['/log-activity/mahasiswa']],
                    ['label' => 'Buku Pedoman', 'icon' => ' fa-file-text-o', 'url' => ['/buku-pedoman/mhs']],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                ],
]
        ) ?>

        <?php } else if ($dosen['id_user'] == $user) { ?>
        <!-- Daftar Menu Untuk Login sebagai Dosen -->
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    // ['label' => 'Menu Yii2', 'options' => ['class' => 'header']],
                    ['label' => 'Daftar Pelanggaran', 'icon' => 'list', 'url' => ['/daftar-pelanggaran/mahasiswa']],
                    ['label' => 'Pelanggaran Mahasiswa', 'icon' => 'dashboard', 'url' => ['/pelanggaran-mahasiswa/dosen']],
                    //['label' => 'Izin Mahasiswa', 'icon' => ' fa-envelope', 'url' => ['/izin-mhs/index']],
                    ['label' => 'Log Activity', 'icon' => ' fa-users', 'url' => ['/log-activity/index']],
                    ['label' => 'Buku Pedoman', 'icon' => ' fa-file-text-o', 'url' => ['/buku-pedoman/mhs']],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                ],
            ]
        ) ?>

        <?php } else if ($security['id_user'] == $user) { ?>
        <!-- Daftar Menu Untuk Login sebagai Security -->
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    // ['label' => 'Menu Yii2', 'options' => ['class' => 'header']],
                    ['label' => 'Daftar Pelanggaran', 'icon' => 'list', 'url' => ['/daftar-pelanggaran/mahasiswa']],
                    ['label' => 'Pelanggaran Mahasiswa', 'icon' => 'dashboard', 'url' => ['/pelanggaran-mahasiswa/index']],
                    //['label' => 'Izin Mahasiswa', 'icon' => ' fa-envelope', 'url' => ['/izin-mhs/index']],
                    //['label' => 'Log Activity', 'icon' => ' fa-users', 'url' => ['/log-activity/index']],
                    ['label' => 'Buku Pedoman', 'icon' => ' fa-file-text-o', 'url' => ['/buku-pedoman/mhs']],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                ],
            ]
        ) ?>

        <?php } else if ($keasramaan['id_user'] == $user) { ?>
        <!-- Daftar Menu Untuk Login sebagai Keasramaan -->
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    // ['label' => 'Menu Yii2', 'options' => ['class' => 'header']],
                    ['label' => 'Daftar Pelanggaran', 'icon' => 'list', 'url' => ['/daftar-pelanggaran/index']],
                    ['label' => 'Pelanggaran Mahasiswa', 'icon' => 'dashboard', 'url' => ['/pelanggaran-mahasiswa/index']],
                    ['label' => 'Penambahan Poin', 'icon' => 'dashboard', 'url' => ['/poin-tambahan/index']],
                    ['label' => 'Izin Mahasiswa', 'icon' => ' fa-envelope', 'url' => ['/izin-mhs/akademik']],
                    ['label' => 'Log Activity', 'icon' => ' fa-users', 'url' => ['/log-activity/index']],
                    ['label' => 'Buku Pedoman', 'icon' => ' fa-file-text-o', 'url' => ['/buku-pedoman/index']],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                ],
            ]
        ) ?>

        <?php } else if ($akademik['id_user'] == $user) { ?>
        <!-- Daftar Menu Untuk Login sebagai Akademik -->
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    // ['label' => 'Menu Yii2', 'options' => ['class' => 'header']],
                    ['label' => 'Daftar Pelanggaran', 'icon' => 'list', 'url' => ['/daftar-pelanggaran/index']],
                    ['label' => 'Pelanggaran Mahasiswa', 'icon' => 'dashboard', 'url' => ['/pelanggaran-mahasiswa/index']],
                    ['label' => 'Approve Pelanggaran', 'icon' => 'dashboard', 'url' => ['/pelanggaran-mahasiswa/akademik']],
                    ['label' => 'Izin Mahasiswa', 'icon' => ' fa-envelope', 'url' => ['/izin-mhs/akademik']],
                    ['label' => 'Log Activity', 'icon' => ' fa-users', 'url' => ['/log-activity/index']],
                    ['label' => 'Buku Pedoman', 'icon' => ' fa-file-text-o', 'url' => ['/buku-pedoman/index']],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                ],
            ]
        ) ?>

        <?php } ?>

    </section>

</aside>
