<?php
use yii\helpers\Html;
use backend\models\BidAkademik;
use backend\models\BidKeasramaan;
use backend\models\Dosen;
use backend\models\Mahasiswa;
use backend\models\Security;


/* @var $this \yii\web\View */
/* @var $content string */
$user = Yii::$app->user->id; 
$mahasiswa = Mahasiswa::find()->where(['id_user' => $user])->one();
$dosen = Dosen::find()->where(['id_user' => $user])->one();
$security = Security::find()->where(['id_user' => $user])->one();
$keasramaan = BidKeasramaan::find()->where(['id_user' => $user])->one();
$akademik = BidAkademik::find()->where(['id_user' => $user])->one();
$test = 2;
?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini">SIAK</span><span class="logo-lg">' . 'SIAK' . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">SIAK</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">

                <!-- User Account: style can be found in dropdown.less -->

                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="user-image" alt="User Image"/>
                        <span class="hidden-xs"> <?php echo $mahasiswa['nama_mhs'] ?>
                            <?php echo $dosen['nama'] ?>
                            <?php echo $security['nama'] ?>
                            <?php echo $akademik['nama'] ?>
                            <?php echo $keasramaan['nama'] ?>
                        </span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle"
                                 alt="User Image"/>

                            <p>
                                <?php echo $mahasiswa['nama_mhs'] ?>
                                <small><?php echo $mahasiswa['status'] ?></small>
                                <?php echo $dosen['nama'] ?>
                                <small><?php echo $dosen['status'] ?></small>
                                <?php echo $security['nama'] ?>
                                <small><?php echo $security['status'] ?></small>
                                <?php echo $keasramaan['nama'] ?>
                                <small><?php echo $keasramaan['status'] ?></small>
                                <?php echo $akademik['nama'] ?>
                                <small><?php echo $akademik['status'] ?></small>

                               <!--  <small> Juni 2018</small> -->
                            </p>
                        </li>

                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <?php if($mahasiswa['id_user'] == $user) { ?>    
                                <?= Html::a(
                                    'Profile',
                                    ['/mahasiswa/view', 'id' => $mahasiswa['nim']],
                                

                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>

                                <?php } else if ($dosen['id_user'] == $user) { ?>
                                <?= Html::a(
                                    'Profile',
                                    ['/dosen/view', 'id' => $dosen['id_dosen']],

                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                                <?php ?>

                                <?php } else if ($security['id_user'] == $user) { ?>
                                <?= Html::a(
                                    'Profile',
                                    ['/security/view', 'id' => $security['id_security']],

                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                                <?php  ?>

                                <?php } else if ($keasramaan['id_user'] == $user) { ?>
                                <?= Html::a(
                                    'Profile',
                                    ['/bid-keasramaan/view', 'id' => $keasramaan['id_keasramaan']],

                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                                <?php  ?>

                                <?php } else if ($akademik['id_user'] == $user) { ?>
                                <?= Html::a(
                                    'Profile',
                                    ['/bid-akademik/view', 'id' => $akademik['id_akademik'], 'id' => $akademik['nik']],

                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                                <?php } ?>


                            </div>
                            <div class="pull-right">
                                <?= Html::a(
                                    'Log out',
                                    ['/site/logout'],
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                        </li>
                    </ul>
                </li>

                <!-- User Account: style can be found in dropdown.less -->
                <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</header>
