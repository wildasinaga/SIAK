<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\LaporanPelanggaran */

$this->title = 'Create Laporan Pelanggaran';
$this->params['breadcrumbs'][] = ['label' => 'Laporan Pelanggarans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="laporan-pelanggaran-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
