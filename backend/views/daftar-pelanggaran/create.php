<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\DaftarPelanggaran */

$this->title = 'Tambah Daftar Pelanggaran';
$this->params['breadcrumbs'][] = ['label' => 'Daftar Pelanggaran', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="daftar-pelanggaran-create">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
