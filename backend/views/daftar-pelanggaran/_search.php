<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\DaftarPelanggaranSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="daftar-pelanggaran-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_pelanggaran') ?>

    <?= $form->field($model, 'jenis_pelanggaran') ?>

    <?= $form->field($model, 'bentuk_pelanggaran') ?>

    <?= $form->field($model, 'nama_pelanggaran') ?>

    <?= $form->field($model, 'poin_pelanggaran') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
