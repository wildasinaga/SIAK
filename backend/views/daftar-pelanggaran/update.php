<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\DaftarPelanggaran */

$this->title = 'Update Daftar Pelanggaran ';
$this->params['breadcrumbs'][] = ['label' => 'Daftar Pelanggarans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_pelanggaran, 'url' => ['view', 'id' => $model->id_pelanggaran]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="daftar-pelanggaran-update">

<p>
</p>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
