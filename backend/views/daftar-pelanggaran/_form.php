<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\JenisPelanggaran;
use backend\models\BentukPelanggaran;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model backend\models\DaftarPelanggaran */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="daftar-pelanggaran-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'jenis_pelanggaran')->dropDownlist(
        ArrayHelper::map(JenisPelanggaran::find()->all(),'id_jenis_pelanggaran', 'nama_jenis_pelanggaran'),
        ['prompt'=>'Pilih Jenis Pelanggaran']
        
        ) ?>

    <?= $form->field($model, 'bentuk_pelanggaran')->dropDownlist(
        ArrayHelper::map(BentukPelanggaran::find()->all(),'id_bentuk_pelanggaran', 'nama_bentuk_pelanggaran'),
        ['prompt'=>'Pilih Bentuk Pelanggaran']
        
        ) ?>

    <?= $form->field($model, 'nama_pelanggaran')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'poin_pelanggaran')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
