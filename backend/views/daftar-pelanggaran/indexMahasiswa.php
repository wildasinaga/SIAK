<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\DaftarPelanggaranSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Daftar Pelanggaran';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="daftar-pelanggaran-index">

    <!--<h1><?= Html::encode($this->title) ?></h1>-->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_pelanggaran',
            [
                'attribute' => 'jenis_pelanggaran',
                'value'=>'jenisPelanggaran.nama_jenis_pelanggaran',
            ],
            [
                'attribute' => 'bentuk_pelanggaran',
                'value'=>'bentukPelanggaran.nama_bentuk_pelanggaran',
            ],
            'nama_pelanggaran:ntext',
            'poin_pelanggaran',

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
