<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\DaftarPelanggaran;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\PelanggaranMahasiswa */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pelanggaran-mahasiswa-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_daftarpelanggaran')->dropDownlist(
        ArrayHelper::map(DaftarPelanggaran::find()->all(),'id_pelanggaran', 'nama_pelanggaran'),
        ['prompt'=>'Pilih Pelanggaran']
        
        ) ?>

    <?= $form->field($model, 'nim')->textInput(['maxlength' => true, 'id'=> 'nimmhs']) ?>

    <?= $form->field($model, 'nama_mahasiswa')->textInput(['maxlength' => true, 'readonly'=> 'readonly']) ?>

    <?= $form->field($model, 'kelas')->textInput(['maxlength' => true, 'readonly'=> 'readonly']) ?>

    <?= $form->field($model, 'deskripsi')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'tanggal_pelanggaran')->widget(
        DatePicker::className([
        'model' => $model,
        'attribute' => 'tanggal_pelanggaran',
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'options' => ['placeholder' => 'Tanggal Pelanggaran ...'],
        'pluginOptions'=> [
            'autoclose'=> true,
            'format'=> 'yyyy-mm-dd'
            ]
        ])

    ) ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

<?php
$script = <<< JS

//here you right all your javascript stuff
$('#nimmhs').change(function(){
    var nimmhs = $(this).val()

    $.get('index.php?r=pelanggaran-mahasiswa/get-nama-mhs-kelas',{nimmhs:nimmhs}, function(data){
        var data = $.parseJSON(data);
        //alert(data.nama_mhs);

        $('#pelanggaranmahasiswa-nama_mahasiswa').attr('value', data.nama_mhs);
        $('#pelanggaranmahasiswa-kelas').attr('value', data.kelas);
    })
    
});

JS;
$this->registerJS($script);
?>

