<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PelanggaranMahasiswaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//Halaman untuk melakukan approval

$this->title = 'Pelanggaran Mahasiswa yang perlu di-approve';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pelanggaran-mahasiswa-index">


   
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider1,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_pelanggaran',
            'id_user',
            [
                'attribute' => 'nim',
                'value'=>'mahasiswa.nama_mhs',
            ],            
            [
                'attribute' => 'id_daftarpelanggaran',
                'value'=>'daftarpelanggaran.nama_pelanggaran',
            ],
            [
                'attribute' => 'id_dosen',
                'value'=>'dosen.nama',
            ],
            //'kelas',
            //'nama_mahasiswa',
            //'deskripsi:ntext',
            //'id_jenis_pelanggaran',
            //'status',
            'tanggal_pelanggaran',
            [
                'attribute' => 'Action',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('Approve', ['/pelanggaran-mahasiswa/approve', 'id'=>$model->id_pelanggaran], ['class' => 'btn btn-primary']);        
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
