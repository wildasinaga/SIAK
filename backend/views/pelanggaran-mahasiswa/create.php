<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\PelanggaranMahasiswa */

$this->title = 'Tambah Pelanggaran Mahasiswa';
$this->params['breadcrumbs'][] = ['label' => 'Pelanggaran Mahasiswas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pelanggaran-mahasiswa-create">

   

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
