<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\PelanggaranMahasiswaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pelanggaran Mahasiswa';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pelanggaran-mahasiswa-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id_pelanggaran',
            // 'id_user',
            [
                'attribute' => 'id_user',
                'value'=>'user.username',
            ],
            'nama_mahasiswa',
            'kelas',

            // [
            //     'attribute' => 'nim',
            //     'value'=>'mahasiswa.nama_mhs',
            // ],            
            [
                'attribute' => 'id_daftarpelanggaran',
                'value'=>'daftarpelanggaran.nama_pelanggaran',
            ],
            [
                'attribute' => 'id_dosen',
                'value'=>'dosen.nama',
            ],
            'deskripsi:ntext',
            //'id_jenis_pelanggaran',
            //'status',
            'tanggal_pelanggaran',

            // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
