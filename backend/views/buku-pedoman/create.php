<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\BukuPedoman */

$this->title = 'Upload Buku Pedoman';
$this->params['breadcrumbs'][] = ['label' => 'Buku Pedoman', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="buku-pedoman-create">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
