<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

$time = new \DateTime('now');
$today = $time->format('m-d-Y');
/* @var $this yii\web\View */
/* @var $model backend\models\BukuPedoman */
/* @var $form yii\widgets\ActiveForm */
$model->tanggal_update = $today;

?>
<div class="buku-pedoman-form">

    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>

    <?= $form->field($model, 'tanggal_update')->widget(
        DatePicker::className([
        'model' => $model,
        'inline'=>false,
        'addon'=>false,
        'disabled'=>!empty($model->tanggal_update),
        'attribute' => 'tanggal_update',
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'options' => ['class'=>'hide', 'readonly'=> 'readonly','placeholder' => 'Tanggal Upload ...'],
        'pluginOptions'=> [
            'autoclose'=> true,
            'format'=> 'yyyy-mm-dd'
            ]
        ])

    ) ?>


    <?= $form->field($model, 'judul')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'file_upload')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
