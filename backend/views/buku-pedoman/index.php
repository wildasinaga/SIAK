<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\BukuPedomanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Buku Pedoman';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="buku-pedoman-index">

    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Upload Buku Pedoman', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_buku',
            'tanggal_update',
            'judul',
            [
            'attribute' => 'file',
            'format' => 'raw',
            'value' => function($model){
                return Html::a('Download File', ['download', 'id' => $model->id_buku], ['class' => '']);
            }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
