<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\BukuPedoman */

$this->title = 'Update Buku Pedoman:';
$this->params['breadcrumbs'][] = ['label' => 'Buku Pedomen', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_buku, 'url' => ['view', 'id' => $model->id_buku]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="buku-pedoman-update">

    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
