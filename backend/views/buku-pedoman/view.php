<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\BukuPedoman */

$this->title = $model->id_buku;
$this->params['breadcrumbs'][] = ['label' => 'Buku Pedomen', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="buku-pedoman-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_buku], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_buku], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_buku',
            'tanggal_update',
            'judul',
            [
            'attribute' => 'file',
            'format' => 'raw',
            'value' => function($model){
                return Html::a('Download File', ['download', 'id' => $model->id_buku], ['class' => '']);
            }
            ]
        ],
    ]) ?>

    <>

</div>
