<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\KategoriIzin */

$this->title = 'Create Kategori Izin';
$this->params['breadcrumbs'][] = ['label' => 'Kategori Izins', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kategori-izin-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
