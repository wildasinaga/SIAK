<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\BidAkademik */

$this->title = 'Update Bid Akademik: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Bid Akademiks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_akademik, 'url' => ['view', 'id_akademik' => $model->id_akademik, 'nik' => $model->nik]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="bid-akademik-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
