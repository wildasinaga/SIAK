<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\BidAkademik */

$this->title = 'Create Bid Akademik';
$this->params['breadcrumbs'][] = ['label' => 'Bid Akademiks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bid-akademik-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
