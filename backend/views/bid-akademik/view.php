<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\BidAkademik */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Bid Akademiks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bid-akademik-view">


    <p>
        <?= Html::a('Update', ['update', 'id_akademik' => $model->id_akademik, 'nik' => $model->nik], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id_akademik' => $model->id_akademik, 'nik' => $model->nik], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_akademik',
            'email:email',
            'nama',
            'nik',
            'id_user',
            'status',
        ],
    ]) ?>

</div>
