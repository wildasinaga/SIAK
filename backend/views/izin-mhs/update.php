<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\IzinMhs */

$this->title = 'Update Izin Mhs';
$this->params['breadcrumbs'][] = ['label' => 'Izin Mhs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->surat_cuti_id, 'url' => ['view', 'id' => $model->surat_cuti_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="izin-mhs-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
