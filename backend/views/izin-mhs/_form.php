<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\BidKeasramaan;
use backend\models\KategoriIzin;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model backend\models\IzinMhs */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="izin-mhs-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_keasramaan')->dropDownlist(
        ArrayHelper::map(BidKeasramaan::find()->all(),'id_keasramaan', 'nama'),
        ['prompt'=>'Pilih keasramaan']
        
        ) ?>

    <?= $form->field($model, 'nim')->textInput(['maxlength' => true, 'id'=> 'nimmhs']) ?>

    <?= $form->field($model, 'nama_mhs')->textInput(['maxlength' => true, 'readonly'=> 'readonly']) ?>

    <?= $form->field($model, 'kelas')->textInput(['maxlength' => true, 'readonly'=> 'readonly']) ?>

    <?= $form->field($model, 'id_kategori')->dropDownlist(
        ArrayHelper::map(KategoriIzin::find()->all(),'id_kategori', 'deskripsi'),
        ['prompt'=>'Pilih kategori...']
        
        ) ?>

    <?= $form->field($model, 'tanggal_berangkat')->widget(
        DatePicker::className([
        'model' => $model,
        'attribute' => 'tanggal_berangkat',
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'options' => ['placeholder' => 'Tanggal Berangkat ...'],
        'pluginOptions'=> [
            'autoclose'=> true,
            'format'=> 'yyyy-mm-dd'
            ]
        ])

    ) ?>


    <?= $form->field($model, 'tanggal_kembali')->widget(
        DatePicker::className([
        'model' => $model,
        'attribute' => 'tanggal_kembali',
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'options' => ['placeholder' => 'Tanggal Kembali ...'],
        'pluginOptions'=> [
            'autoclose'=> true,
            'format'=> 'yyyy-mm-dd'
            ]
        ])

    ) ?>

    <?= $form->field($model, 'alasan')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$script = <<< JS

//here you right all your javascript stuff
$('#nimmhs').change(function(){
    var nimmhs = $(this).val();
    
    $.get('index.php?r=izin-mhs/get-nama-mhs-kelas',{ nimmhs: nimmhs}, function(data){
        var data = $.parseJSON(data);
        //alert(data.nama_mhs);
        $('#izinmhs-nama_mhs').attr('value', data.nama_mhs);
        $('#izinmhs-kelas').attr('value', data.kelas);
        
    });
});

JS;
$this->registerJS($script);
?>
