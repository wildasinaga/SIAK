<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\models\bidKeasramaan;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\IzinMhsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Izin Mahasiswa';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="izin-mhs-index">

    <p>
        <?= Html::a('Tambah Izin Mahasiswa', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'surat_cuti_id',
            [
                'attribute' => 'id_keasramaan',
                'value'=>'keasramaan.nama',
            ],
            'nim',
            'nama_mhs',
            'kelas',
            [
                'attribute' => 'id_kategori',
                'value'=>'kategori.deskripsi',
            ],
            'tanggal_berangkat',
            'tanggal_kembali',
            'alasan',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
