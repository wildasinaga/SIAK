<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\IzinMhs */

$this->title = 'Tambah Izin Mahasiswa';
$this->params['breadcrumbs'][] = ['label' => 'Izin Mhs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="izin-mhs-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
