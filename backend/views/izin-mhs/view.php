<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\IzinMhs */

$this->title = $model->surat_cuti_id;
$this->params['breadcrumbs'][] = ['label' => 'Izin Mhs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="izin-mhs-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->surat_cuti_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->surat_cuti_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'surat_cuti_id',
            'id_keasramaan',
            'nim',
            'nama_mhs',
            'kelas',
            'id_kategori',
            'tanggal_berangkat',
            'tanggal_kembali',
            'alasan',
        ],
    ]) ?>

</div>
