<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "jenis_pelanggaran".
 *
 * @property int $id_jenis_pelanggaran
 * @property string $nama_jenis_pelanggaran
 *
 * @property DaftarPelanggaran[] $daftarPelanggarans
 * @property PelanggaranMahasiswa[] $pelanggaranMahasiswas
 */
class JenisPelanggaran extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'jenis_pelanggaran';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_jenis_pelanggaran'], 'required'],
            [['nama_jenis_pelanggaran'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_jenis_pelanggaran' => 'Id Jenis Pelanggaran',
            'nama_jenis_pelanggaran' => 'Nama Jenis Pelanggaran',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDaftarPelanggarans()
    {
        return $this->hasMany(DaftarPelanggaran::className(), ['jenis_pelanggaran' => 'id_jenis_pelanggaran']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPelanggaranMahasiswas()
    {
        return $this->hasMany(PelanggaranMahasiswa::className(), ['id_jenis_pelanggaran' => 'id_jenis_pelanggaran']);
    }
}
