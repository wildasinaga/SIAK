<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\DaftarPelanggaran;

/**
 * DaftarPelanggaranSearch represents the model behind the search form of `backend\models\DaftarPelanggaran`.
 */
class DaftarPelanggaranSearch extends DaftarPelanggaran
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pelanggaran', 'jenis_pelanggaran', 'bentuk_pelanggaran', 'poin_pelanggaran'], 'integer'],
            [['nama_pelanggaran'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DaftarPelanggaran::find();
        $query->joinWith(['jenisPelanggaran']);
        $query->joinWith(['bentukPelanggaran']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_pelanggaran' => $this->id_pelanggaran,
            'jenis_pelanggaran' => $this->jenis_pelanggaran,
            'bentuk_pelanggaran' => $this->bentuk_pelanggaran,
            'poin_pelanggaran' => $this->poin_pelanggaran,
        ]);

        $query->andFilterWhere(['like', 'nama_pelanggaran', $this->nama_pelanggaran])
              ->andFilterWhere(['like', 'jenis_pelanggaran.nama_jenis_pelanggaran', $this->jenis_pelanggaran])
              ->andFilterWhere(['like', 'bentuk_pelanggaran.nama_bentuk_pelanggaran', $this->bentuk_pelanggaran]);

        return $dataProvider;
    }
}
