<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "bentuk_pelanggaran".
 *
 * @property int $id_bentuk_pelanggaran
 * @property string $nama_bentuk_pelanggaran
 */
class BentukPelanggaran extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bentuk_pelanggaran';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_bentuk_pelanggaran'], 'required'],
            [['nama_bentuk_pelanggaran'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_bentuk_pelanggaran' => 'Id Bentuk Pelanggaran',
            'nama_bentuk_pelanggaran' => 'Nama Bentuk Pelanggaran',
        ];
    }
}
