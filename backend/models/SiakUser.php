<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "siak_user".
 *
 * @property int $id_user
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_user_token
 * @property string $email
 * @property int $status
 * @property int $created_at
 * @property int $update_at
 *
 * @property BidAkademik[] $bidAkademiks
 * @property BidKeasramaan[] $bidKeasramaans
 * @property Dosen[] $dosens
 * @property Mahasiswa[] $mahasiswas
 * @property PelanggaranMahasiswa[] $pelanggaranMahasiswas
 * @property Security[] $securities
 */
class SiakUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'siak_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'auth_key', 'password_hash', 'email'], 'required'],
            [['status', 'created_at', 'update_at'], 'integer'],
            [['username', 'auth_key', 'password_hash', 'password_user_token', 'email'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_user' => 'Id User',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_user_token' => 'Password User Token',
            'email' => 'Email',
            'status' => 'Status',
            'created_at' => 'Created At',
            'update_at' => 'Update At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBidAkademiks()
    {
        return $this->hasMany(BidAkademik::className(), ['id_user' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBidKeasramaans()
    {
        return $this->hasMany(BidKeasramaan::className(), ['id_user' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDosens()
    {
        return $this->hasMany(Dosen::className(), ['id_user' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMahasiswas()
    {
        return $this->hasMany(Mahasiswa::className(), ['id_user' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPelanggaranMahasiswas()
    {
        return $this->hasMany(PelanggaranMahasiswa::className(), ['id_user' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSecurities()
    {
        return $this->hasMany(Security::className(), ['id_user' => 'id_user']);
    }
}
