<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "poin_tambahan".
 *
 * @property int $id_poin_tambahan
 * @property string $nim
 * @property string $nama_mhs
 * @property string $kelas
 * @property string $alasan
 * @property int $poin_tambahan
 *
 * @property Mahasiswa $nim0
 */
class PoinTambahan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'poin_tambahan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nim', 'nama_mhs', 'kelas', 'alasan', 'poin_tambahan'], 'required'],
            [['poin_tambahan'], 'integer'],
            [['nim', 'nama_mhs', 'kelas'], 'string', 'max' => 255],
            [['alasan'], 'string', 'max' => 500],
            [['nim'], 'exist', 'skipOnError' => true, 'targetClass' => Mahasiswa::className(), 'targetAttribute' => ['nim' => 'nim']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_poin_tambahan' => 'Id Poin Tambahan',
            'nim' => 'Nim',
            'nama_mhs' => 'Nama Mhs',
            'kelas' => 'Kelas',
            'alasan' => 'Alasan',
            'poin_tambahan' => 'Poin Tambahan',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNim0()
    {
        return $this->hasOne(Mahasiswa::className(), ['nim' => 'nim']);
    }
}
