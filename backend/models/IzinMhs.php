<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "izin_mhs".
 *
 * @property int $surat_cuti_id
 * @property int $id_keasramaan
 * @property string $nim
 * @property string $nama_mhs
 * @property string $kelas
 * @property int $id_kategori
 * @property string $tanggal_berangkat
 * @property string $tanggal_kembali
 * @property string $alasan
 *
 * @property BidKeasramaan $keasramaan
 * @property KategoriIzin $kategori
 * @property Mahasiswa $nim0
 */
class IzinMhs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'izin_mhs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_keasramaan', 'nim', 'id_kategori', 'tanggal_berangkat', 'tanggal_kembali', 'alasan'], 'required'],
            [['id_keasramaan', 'id_kategori'], 'integer'],
            [['nim', 'nama_mhs', 'kelas', 'alasan'], 'string', 'max' => 255],
            [['tanggal_berangkat', 'tanggal_kembali'], 'string', 'max' => 10],
            [['id_keasramaan'], 'exist', 'skipOnError' => true, 'targetClass' => BidKeasramaan::className(), 'targetAttribute' => ['id_keasramaan' => 'id_keasramaan']],
            [['id_kategori'], 'exist', 'skipOnError' => true, 'targetClass' => KategoriIzin::className(), 'targetAttribute' => ['id_kategori' => 'id_kategori']],
            [['nim'], 'exist', 'skipOnError' => true, 'targetClass' => Mahasiswa::className(), 'targetAttribute' => ['nim' => 'nim']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'surat_cuti_id' => 'Surat Cuti ID',
            'id_keasramaan' => 'Keasramaan',
            'nim' => 'Nim',
            'nama_mhs' => 'Nama Mahasiswa',
            'kelas' => 'Kelas',
            'id_kategori' => 'Kategori',
            'tanggal_berangkat' => 'Tanggal Berangkat',
            'tanggal_kembali' => 'Tanggal Kembali',
            'alasan' => 'Deskripsi',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKeasramaan()
    {
        return $this->hasOne(BidKeasramaan::className(), ['id_keasramaan' => 'id_keasramaan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKategori()
    {
        return $this->hasOne(KategoriIzin::className(), ['id_kategori' => 'id_kategori']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNim0()
    {
        return $this->hasOne(Mahasiswa::className(), ['nim' => 'nim']);
    }
}
