<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "laporan_pelanggaran".
 *
 * @property int $id_laporan
 * @property int $id_mahasiswa
 * @property int $id_dosen
 * @property string $jenis_laporan
 * @property string $tanggal_laporan
 * @property string $deskripsi
 *
 * @property Dosen $dosen
 * @property Mahasiswa $mahasiswa
 */
class LaporanPelanggaran extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'laporan_pelanggaran';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_mahasiswa', 'id_dosen'], 'integer'],
            [['jenis_laporan', 'tanggal_laporan', 'deskripsi'], 'required'],
            [['tanggal_laporan'], 'safe'],
            [['deskripsi'], 'string'],
            [['jenis_laporan'], 'string', 'max' => 255],
            [['id_dosen'], 'exist', 'skipOnError' => true, 'targetClass' => Dosen::className(), 'targetAttribute' => ['id_dosen' => 'id_dosen']],
            [['id_mahasiswa'], 'exist', 'skipOnError' => true, 'targetClass' => Mahasiswa::className(), 'targetAttribute' => ['id_mahasiswa' => 'id_mahasiswa']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_laporan' => 'Id Laporan',
            'id_mahasiswa' => 'Id Mahasiswa',
            'id_dosen' => 'Id Dosen',
            'jenis_laporan' => 'Jenis Laporan',
            'tanggal_laporan' => 'Tanggal Laporan',
            'deskripsi' => 'Deskripsi',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDosen()
    {
        return $this->hasOne(Dosen::className(), ['id_dosen' => 'id_dosen']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMahasiswa()
    {
        return $this->hasOne(Mahasiswa::className(), ['id_mahasiswa' => 'id_mahasiswa']);
    }
}
