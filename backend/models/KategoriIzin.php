<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "kategori_izin".
 *
 * @property int $id_kategori
 * @property string $deskripsi
 *
 * @property IzinMhs[] $izinMhs
 */
class KategoriIzin extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kategori_izin';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_kategori', 'deskripsi'], 'required'],
            [['id_kategori'], 'integer'],
            [['deskripsi'], 'string', 'max' => 255],
            [['id_kategori'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_kategori' => 'Id Kategori',
            'deskripsi' => 'Deskripsi',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIzinMhs()
    {
        return $this->hasMany(IzinMhs::className(), ['id_kategori' => 'id_kategori']);
    }
}
