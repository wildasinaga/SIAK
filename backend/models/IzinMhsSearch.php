<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\IzinMhs;

/**
 * IzinMhsSearch represents the model behind the search form of `backend\models\IzinMhs`.
 */
class IzinMhsSearch extends IzinMhs
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['surat_cuti_id', 'id_keasramaan'], 'integer'],
            [['nim', 'nama_mhs', 'kelas', 'tanggal_berangkat', 'tanggal_kembali', 'alasan', 'id_keasramaan', 'id_kategori', 'id_kategori'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {   
        $time = new \DateTime('now');
        $today = $time->format('m-d-Y');
        $query = IzinMhs::find();
        $query->joinWith(['keasramaan']);
        $query->joinWith(['kategori']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        

        // grid filtering conditions
        $query->andFilterWhere([
            'surat_cuti_id' => $this->surat_cuti_id,
            // 'id_keasramaan' => $this->id_keasramaan,
            // 'id_kategori' => $this->id_kategori,
        ]);

        $query->andFilterWhere(['like', 'nim', $this->nim])
            ->andFilterWhere(['like', 'nama_mhs', $this->nama_mhs])
            ->andFilterWhere(['like', 'kelas', $this->kelas])
            ->andFilterWhere(['like', 'tanggal_berangkat', $this->tanggal_berangkat])
            ->andFilterWhere(['like', 'tanggal_kembali', $this->tanggal_kembali])
            ->andFilterWhere(['like', 'alasan', $this->alasan])
            ->andFilterWhere(['like', 'bid_keasramaan.nama', $this->id_keasramaan])
            ->andFilterWhere(['like', 'kategori_izin.deskripsi', $this->id_kategori]);

        return $dataProvider;
    }
}
