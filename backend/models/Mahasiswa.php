<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "mahasiswa".
 *
 * @property string $nim
 * @property int $id_user
 * @property int $id_keasramaan
 * @property string $nama_mhs
 * @property string $kelas
 * @property int $id_dosen_wali
 * @property int $angkatan
 * @property resource $foto
 * @property int $poin_pelanggaran
 * @property string $status
 *
 * @property IzinMhs[] $izinMhs
 * @property Dosen $dosenWali
 * @property BidKeasramaan $keasramaan
 * @property SiakUser $user
 * @property PelanggaranMahasiswa[] $pelanggaranMahasiswas
 * @property PoinTambahan[] $poinTambahans
 */
class Mahasiswa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mahasiswa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nim', 'id_user', 'nama_mhs', 'kelas', 'id_dosen_wali', 'angkatan', 'foto'], 'required'],
            [['id_user', 'id_keasramaan', 'id_dosen_wali', 'angkatan', 'poin_pelanggaran'], 'integer'],
            [['foto'], 'string'],
            [['nim', 'nama_mhs', 'kelas', 'status'], 'string', 'max' => 255],
            [['nim'], 'unique'],
            [['id_dosen_wali'], 'exist', 'skipOnError' => true, 'targetClass' => Dosen::className(), 'targetAttribute' => ['id_dosen_wali' => 'id_dosen']],
            [['id_keasramaan'], 'exist', 'skipOnError' => true, 'targetClass' => BidKeasramaan::className(), 'targetAttribute' => ['id_keasramaan' => 'id_keasramaan']],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => SiakUser::className(), 'targetAttribute' => ['id_user' => 'id_user']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nim' => 'Nim',
            'id_user' => 'Id User',
            'id_keasramaan' => 'Keasramaan',
            'nama_mhs' => 'Nama Mahasiswa',
            'kelas' => 'Kelas',
            'id_dosen_wali' => 'Dosen Wali',
            'angkatan' => 'Angkatan',
            'foto' => 'Foto',
            'poin_pelanggaran' => 'Poin Pelanggaran',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIzinMhs()
    {
        return $this->hasMany(IzinMhs::className(), ['nim' => 'nim']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDosenWali()
    {
        return $this->hasOne(Dosen::className(), ['id_dosen' => 'id_dosen_wali']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKeasramaan()
    {
        return $this->hasOne(BidKeasramaan::className(), ['id_keasramaan' => 'id_keasramaan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(SiakUser::className(), ['id_user' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPelanggaranMahasiswas()
    {
        return $this->hasMany(PelanggaranMahasiswa::className(), ['nim' => 'nim']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPoinTambahans()
    {
        return $this->hasMany(PoinTambahan::className(), ['nim' => 'nim']);
    }
}
