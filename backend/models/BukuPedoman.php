<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "buku_pedoman".
 *
 * @property int $id_buku
 * @property string $tanggal_update
 * @property string $judul
 * @property string $file
 */
class BukuPedoman extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $file_upload;
    public static function tableName()
    {
        return 'buku_pedoman';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tanggal_update', 'judul'], 'required'],
            [['judul'], 'string', 'max' => 100],
            [['file', 'tanggal_update'], 'string', 'max' => 200],
            [['file_upload'], 'file'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_buku' => 'Id Buku',
            'tanggal_update' => 'Tanggal',
            'judul' => 'Judul',
            'file' => 'File',
        ];
    }
}
