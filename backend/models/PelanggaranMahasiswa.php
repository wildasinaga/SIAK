<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "pelanggaran_mahasiswa".
 *
 * @property int $id_pelanggaran
 * @property int $id_user
 * @property string $nim
 * @property int $id_daftarpelanggaran
 * @property int $id_dosen
 * @property string $kelas
 * @property string $nama_mahasiswa
 * @property string $deskripsi
 * @property int $id_jenis_pelanggaran
 * @property string $status
 * @property string $tanggal_pelanggaran
 *
 * @property Mahasiswa $nim0
 * @property DaftarPelanggaran $daftarpelanggaran
 * @property Dosen $dosen
 * @property JenisPelanggaran $jenisPelanggaran
 * @property SiakUser $user
 */
class PelanggaranMahasiswa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pelanggaran_mahasiswa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'nim', 'id_daftarpelanggaran', 'id_dosen', 'kelas', 'nama_mahasiswa', 'deskripsi', 'id_jenis_pelanggaran', 'tanggal_pelanggaran'], 'required'],
            [['id_user', 'id_daftarpelanggaran', 'id_dosen', 'id_jenis_pelanggaran'], 'integer'],
            [['deskripsi'], 'string'],
            [['kelas', 'nama_mahasiswa', 'nim'], 'string', 'max' => 255],
            [['status'], 'string', 'max' => 16],
            [['tanggal_pelanggaran'], 'string', 'max' => 20],
            [['nim'], 'exist', 'skipOnError' => true, 'targetClass' => Mahasiswa::className(), 'targetAttribute' => ['nim' => 'nim']],
            [['id_daftarpelanggaran'], 'exist', 'skipOnError' => true, 'targetClass' => DaftarPelanggaran::className(), 'targetAttribute' => ['id_daftarpelanggaran' => 'id_pelanggaran']],
            [['id_dosen'], 'exist', 'skipOnError' => true, 'targetClass' => Dosen::className(), 'targetAttribute' => ['id_dosen' => 'id_dosen']],
            [['id_jenis_pelanggaran'], 'exist', 'skipOnError' => true, 'targetClass' => JenisPelanggaran::className(), 'targetAttribute' => ['id_jenis_pelanggaran' => 'id_jenis_pelanggaran']],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => SiakUser::className(), 'targetAttribute' => ['id_user' => 'id_user']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_pelanggaran' => 'Id Pelanggaran',
            'id_user' => 'Petugas',
            'nim' => 'Nim',
            'id_daftarpelanggaran' => 'Pelanggaran',
            'id_dosen' => 'Dosen',
            'kelas' => 'Kelas',
            'nama_mahasiswa' => 'Nama Mahasiswa',
            'deskripsi' => 'Deskripsi',
            'id_jenis_pelanggaran' => 'Id Jenis Pelanggaran',
            'status' => 'Status',
            'tanggal_pelanggaran' => 'Tanggal Pelanggaran',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNim0()
    {
        return $this->hasOne(Mahasiswa::className(), ['nim' => 'nim']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDaftarpelanggaran()
    {
        return $this->hasOne(DaftarPelanggaran::className(), ['id_pelanggaran' => 'id_daftarpelanggaran']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDosen()
    {
        return $this->hasOne(Dosen::className(), ['id_dosen' => 'id_dosen']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenisPelanggaran()
    {
        return $this->hasOne(JenisPelanggaran::className(), ['id_jenis_pelanggaran' => 'id_jenis_pelanggaran']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(SiakUser::className(), ['id_user' => 'id_user']);
    }

    public function getMahasiswa()
    {
        return $this->hasOne(Mahasiswa::className(), ['nim' => 'nim']);
    }
}
