<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "security".
 *
 * @property int $id_security
 * @property string $nik
 * @property string $nama
 * @property int $id_user
 * @property string $status
 *
 * @property SiakUser $user
 */
class Security extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'security';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nik', 'nama', 'id_user'], 'required'],
            [['id_user'], 'integer'],
            [['nik', 'nama', 'status'], 'string', 'max' => 255],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => SiakUser::className(), 'targetAttribute' => ['id_user' => 'id_user']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_security' => 'Id Security',
            'nik' => 'Nik',
            'nama' => 'Nama',
            'id_user' => 'Id User',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(SiakUser::className(), ['id_user' => 'id_user']);
    }
}
