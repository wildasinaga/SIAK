<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "log_activity".
 *
 * @property int $id_log
 * @property int $id_user
 * @property int $nim
 * @property string $tanggal
 * @property string $deskripsi
 * @property string $file_log
 */
class LogActivity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $file_upload;
    public static function tableName()
    {
        return 'log_activity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user'], 'integer'],
            [['tanggal', 'deskripsi'], 'required'],
            [['deskripsi'], 'string'],
            [['tanggal', 'file_log', 'nim'], 'string', 'max' => 255],
            [['file_upload'], 'file'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_log' => 'Id Log',
            'id_user' => 'Id User',
            'nim' => 'Nim',
            'tanggal' => 'Tanggal',
            'deskripsi' => 'Deskripsi',
            'file_log' => 'File Log',
        ];
    }
}
