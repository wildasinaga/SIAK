<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\PoinTambahan;

/**
 * PoinTambahanSearch represents the model behind the search form of `backend\models\PoinTambahan`.
 */
class PoinTambahanSearch extends PoinTambahan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_poin_tambahan', 'poin_tambahan'], 'integer'],
            [['nim', 'nama_mhs', 'kelas', 'alasan'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PoinTambahan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_poin_tambahan' => $this->id_poin_tambahan,
            'poin_tambahan' => $this->poin_tambahan,
        ]);

        $query->andFilterWhere(['like', 'nim', $this->nim])
            ->andFilterWhere(['like', 'nama_mhs', $this->nama_mhs])
            ->andFilterWhere(['like', 'kelas', $this->kelas])
            ->andFilterWhere(['like', 'alasan', $this->alasan]);

        return $dataProvider;
    }
}
