<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "bid_keasramaan".
 *
 * @property int $id_keasramaan
 * @property string $nik
 * @property string $email
 * @property string $nama
 * @property int $id_user
 * @property string $status
 *
 * @property SiakUser $user
 * @property IzinMhs[] $izinMhs
 * @property Mahasiswa[] $mahasiswas
 */
class BidKeasramaan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bid_keasramaan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nik', 'email', 'nama', 'id_user'], 'required'],
            [['id_user'], 'integer'],
            [['nik'], 'string', 'max' => 15],
            [['email', 'nama'], 'string', 'max' => 25],
            [['status'], 'string', 'max' => 255],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => SiakUser::className(), 'targetAttribute' => ['id_user' => 'id_user']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_keasramaan' => 'Id Keasramaan',
            'nik' => 'Nik',
            'email' => 'Email',
            'nama' => 'Nama',
            'id_user' => 'Id User',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(SiakUser::className(), ['id_user' => 'id_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIzinMhs()
    {
        return $this->hasMany(IzinMhs::className(), ['id_keasramaan' => 'id_keasramaan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMahasiswas()
    {
        return $this->hasMany(Mahasiswa::className(), ['id_keasramaan' => 'id_keasramaan']);
    }
}
