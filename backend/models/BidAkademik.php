<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "bid_akademik".
 *
 * @property int $id_akademik
 * @property string $email
 * @property string $nama
 * @property string $nik
 * @property int $id_user
 * @property string $status
 *
 * @property SiakUser $user
 */
class BidAkademik extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bid_akademik';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'nama', 'nik', 'id_user'], 'required'],
            [['id_user'], 'integer'],
            [['email', 'nama'], 'string', 'max' => 25],
            [['nik'], 'string', 'max' => 15],
            [['status'], 'string', 'max' => 255],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => SiakUser::className(), 'targetAttribute' => ['id_user' => 'id_user']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_akademik' => 'Id Akademik',
            'email' => 'Email',
            'nama' => 'Nama',
            'nik' => 'Nik',
            'id_user' => 'Id User',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(SiakUser::className(), ['id_user' => 'id_user']);
    }
}
