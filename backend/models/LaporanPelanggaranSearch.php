<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\LaporanPelanggaran;

/**
 * LaporanPelanggaranSearch represents the model behind the search form of `backend\models\LaporanPelanggaran`.
 */
class LaporanPelanggaranSearch extends LaporanPelanggaran
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_laporan', 'id_mahasiswa', 'id_dosen'], 'integer'],
            [['jenis_laporan', 'tanggal_laporan', 'deskripsi'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LaporanPelanggaran::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_laporan' => $this->id_laporan,
            'id_mahasiswa' => $this->id_mahasiswa,
            'id_dosen' => $this->id_dosen,
            'tanggal_laporan' => $this->tanggal_laporan,
        ]);

        $query->andFilterWhere(['like', 'jenis_laporan', $this->jenis_laporan])
            ->andFilterWhere(['like', 'deskripsi', $this->deskripsi]);

        return $dataProvider;
    }
}
