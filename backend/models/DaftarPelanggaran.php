<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "daftar_pelanggaran".
 *
 * @property int $id_pelanggaran
 * @property int $jenis_pelanggaran
 * @property int $bentuk_pelanggaran
 * @property string $nama_pelanggaran
 * @property int $poin_pelanggaran
 *
 * @property JenisPelanggaran $jenisPelanggaran
 * @property BentukPelanggaran $bentukPelanggaran
 */
class DaftarPelanggaran extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'daftar_pelanggaran';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jenis_pelanggaran', 'bentuk_pelanggaran', 'nama_pelanggaran', 'poin_pelanggaran'], 'required'],
            [['jenis_pelanggaran', 'bentuk_pelanggaran', 'poin_pelanggaran'], 'integer'],
            [['nama_pelanggaran'], 'string'],
            [['jenis_pelanggaran'], 'exist', 'skipOnError' => true, 'targetClass' => JenisPelanggaran::className(), 'targetAttribute' => ['jenis_pelanggaran' => 'id_jenis_pelanggaran']],
            [['bentuk_pelanggaran'], 'exist', 'skipOnError' => true, 'targetClass' => BentukPelanggaran::className(), 'targetAttribute' => ['bentuk_pelanggaran' => 'id_bentuk_pelanggaran']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_pelanggaran' => 'Id Pelanggaran',
            'jenis_pelanggaran' => 'Jenis Pelanggaran',
            'bentuk_pelanggaran' => 'Bentuk Pelanggaran',
            'nama_pelanggaran' => 'Nama Pelanggaran',
            'poin_pelanggaran' => 'Poin Pelanggaran',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJenisPelanggaran()
    {
        return $this->hasOne(JenisPelanggaran::className(), ['id_jenis_pelanggaran' => 'jenis_pelanggaran']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBentukPelanggaran()
    {
        return $this->hasOne(BentukPelanggaran::className(), ['id_bentuk_pelanggaran' => 'bentuk_pelanggaran']);
    }
}
