<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\PelanggaranMahasiswa;

/**
 * PelanggaranMahasiswaSearch represents the model behind the search form of `backend\models\PelanggaranMahasiswa`.
 */
class PelanggaranMahasiswaSearch extends PelanggaranMahasiswa
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_pelanggaran', 'id_user', 'id_daftarpelanggaran', 'id_dosen', 'id_jenis_pelanggaran'], 'integer'],
            [['nim', 'kelas', 'nama_mahasiswa', 'deskripsi', 'status', 'tanggal_pelanggaran', 'id_user'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PelanggaranMahasiswa::find();
        $query->joinWith(['user']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_pelanggaran' => $this->id_pelanggaran,
            'id_user' => $this->id_user,
            'id_daftarpelanggaran' => $this->id_daftarpelanggaran,
            'id_dosen' => $this->id_dosen,
            'id_jenis_pelanggaran' => $this->id_jenis_pelanggaran,
        ]);

        $query->andFilterWhere(['like', 'nim', $this->nim])
            ->andFilterWhere(['like', 'kelas', $this->kelas])
            ->andFilterWhere(['like', 'nama_mahasiswa', $this->nama_mahasiswa])
            ->andFilterWhere(['like', 'deskripsi', $this->deskripsi])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'tanggal_pelanggaran', $this->tanggal_pelanggaran]);

        return $dataProvider;
    }

    public function searchakademik($params)
    {
        $this->status=$params;
        $query = PelanggaranMahasiswa::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_pelanggaran' => $this->id_pelanggaran,
            'id_user' => $this->id_user,
            'id_daftarpelanggaran' => $this->id_daftarpelanggaran,
            'id_dosen' => $this->id_dosen,
            'id_jenis_pelanggaran' => $this->id_jenis_pelanggaran,
        ]);

        $query->andFilterWhere(['like', 'nim', $this->nim])
            ->andFilterWhere(['like', 'kelas', $this->kelas])
            ->andFilterWhere(['like', 'nama_mahasiswa', $this->nama_mahasiswa])
            ->andFilterWhere(['like', 'deskripsi', $this->deskripsi])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'tanggal_pelanggaran', $this->tanggal_pelanggaran])
            ->andFilterWhere(['like', 'siak_user.username', $this->id_user]);

        return $dataProvider;
    }

    public function searchdosen($params)
    {
        $this->id_dosen=$params;
        $query = PelanggaranMahasiswa::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_pelanggaran' => $this->id_pelanggaran,
            'id_user' => $this->id_user,
            'id_daftarpelanggaran' => $this->id_daftarpelanggaran,
            'id_dosen' => $this->id_dosen,
            'id_jenis_pelanggaran' => $this->id_jenis_pelanggaran,
        ]);

        $query->andFilterWhere(['like', 'nim', $this->nim])
            ->andFilterWhere(['like', 'kelas', $this->kelas])
            ->andFilterWhere(['like', 'nama_mahasiswa', $this->nama_mahasiswa])
            ->andFilterWhere(['like', 'deskripsi', $this->deskripsi])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'tanggal_pelanggaran', $this->tanggal_pelanggaran])
            ->andFilterWhere(['like', 'siak_user.username', $this->id_user]);

        return $dataProvider;
    }
}
