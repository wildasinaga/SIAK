<?php

namespace backend\controllers;

use Yii;
use backend\models\PelanggaranMahasiswa;
use backend\models\PelanggaranMahasiswaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\Mahasiswa;
use backend\models\Dosen;
use backend\models\DaftarPelanggaran;
use backend\models\Security;
use yii\data\ActiveDataProvider;
use backend\models\BidKeasramaan;
use backend\models\BidAkademik;
use yii\helpers\Json;

/**
 * PelanggaranMahasiswaController implements the CRUD actions for PelanggaranMahasiswa model.
 */
class PelanggaranMahasiswaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PelanggaranMahasiswa models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PelanggaranMahasiswaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAkademik()
    {
        $searchModel = new PelanggaranMahasiswaSearch();

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider1 = $searchModel->searchakademik('Not Approve');

        return $this->render('indexAkademik', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'dataProvider1' => $dataProvider1,
        ]);
    }


    public function actionDosen()
    {
        $searchModel = new PelanggaranMahasiswaSearch();
        $user = Yii::$app->user->id; $dosen = Dosen::find()->where(['id_user' => $user])->one(); $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider1 = $searchModel->searchdosen($dosen['id_dosen']);

        return $this->render('indexDosen', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'dataProvider1' => $dataProvider1,
        ]);
    }

    public function actionApprove($id) {
        $model = $this->findModel($id);
        $mahasiswa = Mahasiswa::find()->where(['nim' => $model->nim])->one();
        $pelanggaran = DaftarPelanggaran::find()->where(['id_pelanggaran' => $model->id_daftarpelanggaran])->one();

        $poin_sisa = $mahasiswa['poin_pelanggaran'] - $pelanggaran['poin_pelanggaran'];
        $mahasiswa->poin_pelanggaran = $poin_sisa;
        $model->status = "Approve";
        $model->save(false);
        $mahasiswa->save(false);

        $searchModel = new PelanggaranMahasiswaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider1 = $searchModel->searchakademik('Not Approve');

        return $this->render('indexAkademik', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'dataProvider1' => $dataProvider1,
        ]);
    }

    public function actionMahasiswa()
    {
        $searchModel = new PelanggaranMahasiswaSearch();

        $user = Yii::$app->user->id;
        $mahasiswa = Mahasiswa::find()->where(['id_user' => $user])->one();
        $pelanggaranMahasiswa = PelanggaranMahasiswa::find()->where(['nim'=>$mahasiswa['nim']]);

        $dataProvider = new ActiveDataProvider(
            [
                'query' => $pelanggaranMahasiswa,
                'pagination' =>[
                    'pageSize' => 10
                ],
            ]
        );

        return $this->render('indexMahasiswa', [
            'searchModel' => $searchModel,
            'pelanggaranMahasiswa' => $pelanggaranMahasiswa,
            'dataProvider' => $dataProvider,
        ]);

    }




    /**
     * Displays a single PelanggaranMahasiswa model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PelanggaranMahasiswa model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PelanggaranMahasiswa(); 
        $user = Yii::$app->user->id;
        $dosen = Dosen::find()->where(['id_user' => $user])->one();
        $security = Security::find()->where(['id_user' => $user])->one();
        $keasramaan = BidKeasramaan::find()->where(['id_user' => $user])->one();
        $akademik = BidAkademik::find()->where(['id_user' => $user])->one();

        if($dosen['id_user'] == $user){
            $id_user = $user;
        }elseif ($security['id_user'] == $user) {
            $id_user = $user;
        }elseif ($keasramaan['id_user'] == $user) {
            $id_user = $user;
        }else{
            $id_user = $user;
        }

        if ($model->load(Yii::$app->request->post())) {
            $mahasiswa = Mahasiswa::find()->where(['nim' => $model->nim])->one();
            $pelanggaran = DaftarPelanggaran::find()->where(['id_pelanggaran' => $model->id_daftarpelanggaran])->one();

            if($pelanggaran['jenis_pelanggaran'] == 3){
                $model->id_user = $id_user;
                $model->id_dosen = $mahasiswa['id_dosen_wali'];
                $model->id_jenis_pelanggaran = $pelanggaran['jenis_pelanggaran'];
                $model->status = "Not Approve";
                $model->save();
            }else{
                $poin_sisa = $mahasiswa['poin_pelanggaran'] - $pelanggaran['poin_pelanggaran'];
                $mahasiswa->poin_pelanggaran = $poin_sisa;
                $model->id_user = $id_user;
                $model->id_dosen = $mahasiswa['id_dosen_wali'];
                $model->id_jenis_pelanggaran = $pelanggaran['jenis_pelanggaran'];
                $model->status = "Approve";
                $model->save();
                $mahasiswa->save(false);
            }
            return $this->redirect(['view', 'id' => $model->id_pelanggaran]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing PelanggaranMahasiswa model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_pelanggaran]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionGetNamaMhsKelas($nimmhs){

        //mencari nim di dalam tabel mahasiswa
        $mahasiswa=Mahasiswa::find('nama_mhs', 'kelas')->where(['nim'=>$nimmhs])->one();
        echo json::encode($mahasiswa);
    }

    /**
     * Deletes an existing PelanggaranMahasiswa model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PelanggaranMahasiswa model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PelanggaranMahasiswa the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PelanggaranMahasiswa::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
