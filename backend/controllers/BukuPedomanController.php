<?php

namespace backend\controllers;

use Yii;
use backend\models\BukuPedoman;
use backend\models\BukuPedomanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * BukuPedomanController implements the CRUD actions for BukuPedoman model.
 */
class BukuPedomanController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BukuPedoman models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BukuPedomanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BukuPedoman model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */

    public function actionMhs()
    {
        $searchModel = new BukuPedomanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('indexMhs', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new BukuPedoman model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new BukuPedoman();

        if ($model->load(Yii::$app->request->post())) {
            $model->file_upload = UploadedFile::getInstance($model, 'file_upload');
            $model->file_upload->saveAs('uploads/' .$model->file_upload->baseName. '.'.$model->file_upload->extension);
            //save the path in the db column
            $model->file = 'uploads/'.$model->file_upload->baseName. '.'.$model->file_upload->extension;
            $model->save();
            return $this->redirect(['view', 'id' => $model->id_buku]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionDownload($id){
        $model=$this->findModel($id);

        /*$model2=Model::find()->where(['id_user'=>$id])->all();
        $jlh_transaksi=count($model2);
        if($jlh_transaksi>10){
        }*/
        $source_path = Yii::getAlias('@webroot/');
        $file=$source_path.$model['file'];
        // var_dump($file);die();
        //var_dump($file);
        //die();
        if (file_exists($file)) {
            Yii::$app->response->sendFile($file);
      }
    }
    /**
     * Updates an existing BukuPedoman model.
     * If update is successful, the browser will be redirected to the kkkl\'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_buku]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing BukuPedoman model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the BukuPedoman model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BukuPedoman the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BukuPedoman::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
