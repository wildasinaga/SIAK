<?php

namespace backend\controllers;

use Yii;
use backend\models\LogActivity;
use backend\models\LogActivitySearch;
use backend\models\Mahasiswa;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\data\ActiveDataProvider;

/**
 * LogActivityController implements the CRUD actions for LogActivity model.
 */
class LogActivityController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LogActivity models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LogActivitySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LogActivity model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new LogActivity model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LogActivity();

        if ($model->load(Yii::$app->request->post())) {
            $user=Mahasiswa::find()->where (['id_user'=>Yii::$app->user->id])->one();
            $model->id_user=Yii::$app->user->id;
            $model->nim=$user['nim'];

            $model->file_upload = UploadedFile::getInstance($model, 'file_upload');
            $model->file_upload->saveAs('uploads/logactivity/' .$model->file_upload->baseName. '.'.$model->file_upload->extension);

            //save the path in the db column
            $model->file_log = 'uploads/logactivity/'.$model->file_upload->baseName. '.'.$model->file_upload->extension;
            $model->save();

            return $this->redirect(['view', 'id' => $model->id_log]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionDownload($id){
        $model=$this->findModel($id);

        /*$model2=Model::find()->where(['id_user'=>$id])->all();
        $jlh_transaksi=count($model2);
        if($jlh_transaksi>10){
        }*/
        $source_path = Yii::getAlias('@webroot/');
        $file=$source_path.$model['file_log'];
        // var_dump($file);die();
        //var_dump($file);
        //die();
        if (file_exists($file)) {
            Yii::$app->response->sendFile($file);
      }
    }

    public function actionMahasiswa()
    {
        $searchModel = new LogActivitySearch();

        $user = Yii::$app->user->id;
        $mahasiswa = LogActivity::find()->where(['id_user' => $user])->one();
        $logactivity = LogActivity::find()->where(['nim'=>$mahasiswa['nim']]);

        $dataProvider = new ActiveDataProvider(
            [
                'query' => $logactivity,
                'pagination' =>[
                    'pageSize' => 10
                ],
            ]
        );

        return $this->render('indexMahasiswa', [
            'searchModel' => $searchModel,
            'pelanggaranMahasiswa' => $logactivity,
            'dataProvider' => $dataProvider,
        ]);

    }

    /**
     * Updates an existing LogActivity model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_log]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing LogActivity model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the LogActivity model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LogActivity the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LogActivity::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
