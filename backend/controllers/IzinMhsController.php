<?php

namespace backend\controllers;

use Yii;
use backend\models\IzinMhs;
use backend\models\IzinMhsSearch;
use backend\models\Mahasiswa;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\data\ActiveDataProvider;



/**
 * IzinMhsController implements the CRUD actions for IzinMhs model.
 */
class IzinMhsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all IzinMhs models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new IzinMhsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIzinMahasiswa(){
        $time = new \DateTime('now');
        $today = $time->format('m/d/Y');
        // var_dump($today);;die();
        $query = IzinMhs::find()->where(['>=', 'tanggal_berangkat', $today]);
        

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->render('index_izin_mahasiswa', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAkademik()
    {
        $searchModel = new IzinMhsSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('indexakademik', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single IzinMhs model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new IzinMhs model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new IzinMhs();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->surat_cuti_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing IzinMhs model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->surat_cuti_id]);
        }



        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionGetNamaMhsKelas($nimmhs){

        //mencari nim di dalam tabel mahasiswa
        $mahasiswa=Mahasiswa::find('nama_mhs', 'kelas')->where(['nim'=>$nimmhs])->one();
        echo json::encode($mahasiswa);
    }




    /**
     * Deletes an existing IzinMhs model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the IzinMhs model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return IzinMhs the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = IzinMhs::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
