<?php

namespace backend\controllers;

use Yii;
use backend\models\BidAkademik;
use backend\models\BidAkademikSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BidAkademikController implements the CRUD actions for BidAkademik model.
 */
class BidAkademikController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all BidAkademik models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BidAkademikSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single BidAkademik model.
     * @param integer $id_akademik
     * @param string $nik
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id_akademik, $nik)
    {
        return $this->render('view', [
            'model' => $this->findModel($id_akademik, $nik),
        ]);
    }

    /**
     * Creates a new BidAkademik model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new BidAkademik();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id_akademik' => $model->id_akademik, 'nik' => $model->nik]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing BidAkademik model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id_akademik
     * @param string $nik
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id_akademik, $nik)
    {
        $model = $this->findModel($id_akademik, $nik);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id_akademik' => $model->id_akademik, 'nik' => $model->nik]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing BidAkademik model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id_akademik
     * @param string $nik
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id_akademik, $nik)
    {
        $this->findModel($id_akademik, $nik)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the BidAkademik model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id_akademik
     * @param string $nik
     * @return BidAkademik the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id_akademik, $nik)
    {
        if (($model = BidAkademik::findOne(['id_akademik' => $id_akademik, 'nik' => $nik])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
