<?php

namespace backend\controllers;

use Yii;
use backend\models\PoinTambahan;
use backend\models\Mahasiswa;
use backend\models\PoinTambahanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;

/**
 * PoinTambahanController implements the CRUD actions for PoinTambahan model.
 */
class PoinTambahanController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all PoinTambahan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PoinTambahanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PoinTambahan model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PoinTambahan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PoinTambahan();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            
            $user=Mahasiswa::find()->where (['nim'=>$model->nim])->one();
            $user->poin_pelanggaran=$user->poin_pelanggaran + $model->poin_tambahan;
            $connection = Yii::$app->db;
            $connection->createCommand()->update('mahasiswa',['poin_pelanggaran'=>$user->poin_pelanggaran], ['nim'=>$model->nim])->execute();
            return $this->redirect(['view', 'id' => $model->id_poin_tambahan]);
        }

        

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing PoinTambahan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_poin_tambahan]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    // public function actionGetNamaMhsKelas($nimmhs){

    //     //mencari nim di dalam tabel mahasiswa
    //     $mahasiswa=Mahasiswa::find('nama_mhs', 'kelas')->where(['nim'=>$nimmhs])->one();
    //     echo json::encode($mahasiswa);
    // }

    public function actionGetNamaMhsKelas($nimmahasiswa){
        //find nim mahasiswa dari db
        $mahasiswa = Mahasiswa::find('nama_mhs', '')->where(['nim'=>$nimmahasiswa])->one();
        echo Json::encode($mahasiswa);
    }

    /**
     * Deletes an existing PoinTambahan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PoinTambahan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PoinTambahan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PoinTambahan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
