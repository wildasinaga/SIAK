-----------------------SIAK-----------------------
-                                                -
-----Sistem Informasi Akademik dan Keasramaan-----
-                                                -
--------------------------------------------------

Proyek ini dikembangkan oleh PSI-18-05 dengan yii2 framework advanced
1. Wilda Sinaga - 12S15050
2. Novi Gultom - 12S15046
3. Veronika Sinaga - 12S15010
4. Putri Manurung - 12S15021

Cara menggunakan proyek ini :
1. Pindahkan file ke directory Local Disk(C:)->XAMPP->htdocs
1. Buat database baru dengan nama "siakbaru". Kemudian import file siakbaru.sql ke database yang baru diimport
2. Nyalakan XAMPP -> Start Apache dan MySQL
3. Pergi ke browser, ketikkan localhost:/siak

Untuk melakukan register di proyek:
1. Melalui browser, ketikkan halaman localhost:/siak/frontend/web/index.php?
2. Pilih Menu Sign Up.
3. Isi form kemudian tekan tombol "Signup"

Setelah melakukan sign up:
1. Pada browser buka localhost:/phpmyadmin atau aplikasi SQLYog
2. Buka database siakbaru
3. Buka tabel siak_user
4. Salin username yang baru didaftarkan kemudian paste ke salah satu tabel apakah 
   akun tersebut bidang akademik, bidang keasramaan, mahasiswa, dosen atau security.
5. Lengkapi data tersebut pada tabel yang sudah dipilih

ROLE :
Ada 5 role pada sistem ini yaitu :
- bidang akademik
- bidang keasramaan
- dosen
- security 
- mahasiswa

LOGIN (menggunakan user yang sudah ada) :
untuk melakukan login dengan user yang sudah ada, bisa lihat daftar user pada tabel bid_akademik, bid_keasramaan, dosen, mahasiswa, security
username : "nama sesuai pada tabel"
password : "nama awal"123

pada password, gunakan nama awal ditambah 123 (huruf kecil semua)